package com.limocar.driver.activities;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.view.View;
import android.view.Window;

import com.limocar.driver.AppDelegate;
import com.limocar.driver.R;
import com.limocar.driver.TransitionHelper;
import com.limocar.driver.service.LocationService;
import com.limocar.driver.utils.Prefs;


public class SplashActivity extends Activity {

    // Global variable declaration
    protected int mSplashTime = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // To remove Action bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        // inflate layout xml file for Splash Activity
        setContentView(R.layout.activity_splash_02);
        startService(new Intent(this, LocationService.class));


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if ( new Prefs(SplashActivity.this).getUserdata() != null) {
                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(SplashActivity.this, SignInActivity.class);
                    final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(SplashActivity.this, false,
                            new Pair<>(findViewById(R.id.img_logo), getString(R.string.img_logo)),
                            new Pair<>(findViewById(R.id.img_right_top), getString(R.string.img_right_top)));
                    ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(SplashActivity.this, pairs);
                    startActivity(intent, transitionActivityOptions.toBundle());
                    finish();
                }
            }
        }, 1000);
    }

    // when user press the back button then only current thread would be interrupted
    @Override
    public void onBackPressed() {
    }

}
