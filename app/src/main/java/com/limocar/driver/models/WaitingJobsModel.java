package com.limocar.driver.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Heena on 03-Feb-17.
 */

public class WaitingJobsModel implements Parcelable {
    public int status;

    protected WaitingJobsModel(Parcel in) {
        status = in.readInt();
    }

    public static final Creator<WaitingJobsModel> CREATOR = new Creator<WaitingJobsModel>() {
        @Override
        public WaitingJobsModel createFromParcel(Parcel in) {
            return new WaitingJobsModel(in);
        }

        @Override
        public WaitingJobsModel[] newArray(int size) {
            return new WaitingJobsModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(status);
    }
}
