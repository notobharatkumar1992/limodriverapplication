package com.limocar.driver.fragments;//package com.limocar.passenger.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Bitmap;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.limocar.driver.AppDelegate;
import com.limocar.driver.Async.LocationAddress;
import com.limocar.driver.R;
import com.limocar.driver.activities.MainActivity;
import com.limocar.driver.activities.SignInActivity;
import com.limocar.driver.constants.Constants;
import com.limocar.driver.constants.Tags;
import com.limocar.driver.net.Callback;
import com.limocar.driver.net.RestError;
import com.limocar.driver.net.SingletonRestClient;
import com.limocar.driver.utils.CircleImageView;
import com.limocar.driver.utils.Prefs;
import com.limocar.driver.utils.WorkaroundMapFragment;

import org.json.JSONException;
import org.json.JSONObject;

import carbon.widget.ImageView;
import carbon.widget.LinearLayout;
import carbon.widget.RelativeLayout;
import carbon.widget.TextView;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Bharat on 07/29/2016.
 */
public class HomeFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener, View.OnClickListener {

    private String name = "", country = "", state = "", city = "", image = "", company_name = "";
    private Handler mHandler;
    private TextView txt_c_title;
    ImageView img_c_loading1;
    CircleImageView cimg_user;
    private Marker customMarker;
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private double currentLatitude = 0, currentLongitude = 0;
    WorkaroundMapFragment map;
    TextView txt_c_address;
    LinearLayout ll_c_;
    private SupportMapFragment fragment;
    private GoogleMap googleMap;
    android.widget.RelativeLayout rl_fare;
    TextView txt_c_submit, txt_c_pick_up, txt_c_drop_off;

    @Nullable
    @Override

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getActivity().getWindow();
            Drawable background = getActivity().getResources().getDrawable(R.color.view_border_color);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        return inflater.inflate(R.layout.search_loaction_dashboard, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        ((MainActivity) getActivity()).setInitailSideBar(MainActivity.PANEL_HOME);
        initGPS();
        showGPSalert();
        setHandler();
        Constants.isMapScreen=true;
    }


    private void showGPSalert() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            mGoogleApiClient.connect();
        }
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        //**************************
        builder.setAlwaysShow(true); //this is the key ingredient
        //**************************
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(
                                    getActivity(), 1000);
                        } catch (IntentSender.SendIntentException e) {
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;
                }
            }
        });
    }

    private void initGPS() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        AppDelegate.LogT("mGoogleApiClient Initialited");
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(500)        // 10 seconds, in milliseconds
                .setFastestInterval(500); // 10 second, in milliseconds
        AppDelegate.LogT("mLocationRequest Initialited");
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient,
                        HomeFragment.this);
                mGoogleApiClient.disconnect();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void initView(View view) {
        txt_c_title = (TextView) view.findViewById(R.id.txt_c_title);
        rl_fare = (android.widget.RelativeLayout) view.findViewById(R.id.rl_fare);
        txt_c_submit = (TextView) view.findViewById(R.id.txt_c_submit);
        txt_c_submit.setOnClickListener(this);
        view.findViewById(R.id.img_clr_txt).setOnClickListener(this);
        txt_c_pick_up = (TextView) view.findViewById(R.id.txt_c_pick_up);
        txt_c_drop_off = (TextView) view.findViewById(R.id.txt_c_drop_off);
        new Handler().postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        try {
                            fragment = SupportMapFragment.newInstance();
                            getChildFragmentManager().beginTransaction()
                                    .replace(R.id.map_container, fragment, "MAP1").addToBackStack(null)
                                    .commit();
                            fragment.getMapAsync(
                                    new OnMapReadyCallback() {
                                        @Override
                                        public void onMapReady(GoogleMap googleMap) {
                                            HomeFragment.this.googleMap = googleMap;
                                            AppDelegate.LogT("Google Map ==" + HomeFragment.this.googleMap);
                                            showMap();

                                        }
                                    }
                            );
                        } catch (Exception e) {
                            AppDelegate.LogE(e);
                        }
                    }
                }

                , 300);

    }

    private void showMap() {
        if (googleMap == null) {
            return;
        }
        googleMap.setMyLocationEnabled(true);
        googleMap.getUiSettings().setMapToolbarEnabled(false);
        googleMap.getUiSettings().setZoomControlsEnabled(false);
        googleMap.getUiSettings().setCompassEnabled(false);
        googleMap.getUiSettings().setRotateGesturesEnabled(false);
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.clear();
        createMarker();
        new Handler(getActivity().getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                AppDelegate.setStictModePermission();
                CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(currentLatitude, currentLongitude))
                        .zoom(5).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                setloc(currentLatitude, currentLongitude);
            }
        });
        googleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                currentLatitude = cameraPosition.target.latitude;
                currentLongitude = cameraPosition.target.longitude;
                setloc(currentLatitude, currentLongitude);
                createMarker();
            }
        });
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(getActivity());
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(getActivity());
                        break;
                    case 2:
                        setAddressFromGEOcoder(msg.getData());
                        break;
                    case 3:
                        if (txt_c_submit.getText().toString().equalsIgnoreCase(getString(R.string.start_trip))) {
                            callStartTripApi();
                        } else {
                            callStopTripApi();
                        }
                        break;
                }
            }
        };
    }

    private void callStartTripApi() {
        rl_fare.setVisibility(View.GONE);
        txt_c_submit.setText(getString(R.string.stop_trip));
        txt_c_submit.setBackgroundResource(R.drawable.bg_orange_button);
        if (AppDelegate.haveNetworkConnection(getActivity())) {
            mHandler.sendEmptyMessage(10);
            SingletonRestClient.get().startTrip(AppDelegate.session,new Callback<Response>() {

                @Override
                public void failure(RestError restError) {
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                        AppDelegate.showToast(getActivity(), obj_json.getString(Tags.m));
                    } catch (JSONException e) {
                        AppDelegate.LogE(e);
                    }
                }

                @Override
                public void success(Response response, Response response2) {
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                        txt_c_submit.setText(getString(R.string.stop_trip));
                        txt_c_submit.setBackgroundResource(R.drawable.bg_orange_button);
                        if (obj_json.getString(Tags.s).contains("32")) {
                            AppDelegate.showToast(getActivity(), obj_json.getString(Tags.m));
                        } else {
                            AppDelegate.showAlert(getActivity(), obj_json.getString(Tags.extra));
                        }
                    } catch (JSONException e) {
                        AppDelegate.LogE(e);
                    }
                }
            });
        }
    }

    private void callStopTripApi() {
        rl_fare.setVisibility(View.VISIBLE);
        txt_c_submit.setText(getString(R.string.start_trip));
        txt_c_submit.setBackgroundResource(R.drawable.bg_orange_button);
        AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), new TripReceiptFragment(), R.id.main_containt);
        if (AppDelegate.haveNetworkConnection(getActivity())) {
            mHandler.sendEmptyMessage(10);
            SingletonRestClient.get().stopTrip(AppDelegate.session,new Callback<Response>() {
                @Override
                public void failure(RestError restError) {
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                        AppDelegate.showToast(getActivity(), obj_json.getString(Tags.m));
                    } catch (JSONException e) {
                        AppDelegate.LogE(e);
                    }
                }

                @Override
                public void success(Response response, Response response2) {
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                        txt_c_submit.setText(getString(R.string.start_trip));
                        txt_c_submit.setBackgroundResource(R.drawable.bg_orange_button);
                        if (obj_json.getString(Tags.s).contains("32")) {
                            AppDelegate.showToast(getActivity(), obj_json.getString(Tags.m));
                        } else {
                            AppDelegate.showAlert(getActivity(), obj_json.getString(Tags.extra));
                        }
                    } catch (JSONException e) {
                        AppDelegate.LogE(e);
                    }
                }
            });
        }
    }


    private void setAddressFromGEOcoder(Bundle data) {
        currentLatitude = data.getDouble(Tags.LAT);
        currentLatitude = data.getDouble(Tags.LNG);
        txt_c_pick_up.setText(data.getString(Tags.ADDRESS));
    }

    @Override
    public void onConnected(Bundle bundle) {
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        AppDelegate.LogT("onConnected Initialited");
        if (location == null) {
            try {
                AppDelegate.LogT("onConnected Initialited== null");
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, HomeFragment.this);
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        } else {
            currentLatitude = location.getLatitude();
            currentLongitude = location.getLongitude();
            setloc(currentLatitude, currentLongitude);
            new Prefs(getActivity()).putStringValue(Tags.LAT, String.valueOf(currentLatitude));
            new Prefs(getActivity()).putStringValue(Tags.LNG, String.valueOf(currentLongitude));

        }
    }

    private void setloc(double latitude, double longitude) {
        LocationAddress.getAddressFromLocation(latitude, longitude, getActivity(), mHandler);
    }

    public void createMarker() {
        googleMap.clear();
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (status == ConnectionResult.SUCCESS) {
            //Success! Do what you want
            try {
                View marker = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker, null);
                final android.widget.ImageView cimg_user = (android.widget.ImageView) marker.findViewById(R.id.imageView);
                cimg_user.setImageResource(R.drawable.marker_green);
                googleMap.addMarker(new MarkerOptions().position(new LatLng(currentLatitude, currentLongitude))
                        .title("Pickup")
                        .icon(BitmapDescriptorFactory.fromBitmap(AppDelegate.createDrawableFromView(getActivity(), marker))));
                cimg_user.setImageResource(R.drawable.marker_orange);
                googleMap.addMarker(new MarkerOptions().position(new LatLng(25.1211212, 74.78878451))
                        .title("Drop off")
                        .icon(BitmapDescriptorFactory.fromBitmap(AppDelegate.createDrawableFromView(getActivity(), marker))));
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            GooglePlayServicesUtil.getErrorDialog(status, getActivity(), status);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                AppDelegate.LogT("onConnectionFailed Initialited" + connectionResult);
                connectionResult.startResolutionForResult(getActivity(), CONNECTION_FAILURE_RESOLUTION_REQUEST);

            } catch (IntentSender.SendIntentException e) {
                AppDelegate.LogE(e);
            }
        } else {
            Log.e("Error", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        AppDelegate.hideKeyBoard(getActivity());
        mGoogleApiClient.connect();

    }

    @Override
    public void onLocationChanged(Location location) {
        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();
        new Prefs(getActivity()).putStringValue(Tags.LAT, String.valueOf(currentLatitude));
        new Prefs(getActivity()).putStringValue(Tags.LNG, String.valueOf(currentLongitude));
        try {
            LocationAddress.getAddressFromLocation(currentLatitude, currentLongitude, getActivity(), mHandler);
            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, HomeFragment.this);
                mGoogleApiClient.disconnect();
                AppDelegate.LogGP("Fused Location api disconnect called");
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_c_submit:
                mHandler.sendEmptyMessage(3);
                break;
            case R.id.img_clr_txt:
                txt_c_drop_off.setText("");
                break;
        }

    }
}
