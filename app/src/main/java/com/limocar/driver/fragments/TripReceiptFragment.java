package com.limocar.driver.fragments;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.limocar.driver.R;
import com.limocar.driver.activities.MainActivity;
import com.limocar.driver.adapters.WaitingJobsAdapter;
import com.limocar.driver.models.WaitingJobsModel;

import java.util.ArrayList;

import carbon.widget.TextView;


public class TripReceiptFragment extends Fragment implements View.OnClickListener {
    RecyclerView waiting_jobs;
    private WaitingJobsAdapter mAdapter;
    ArrayList<WaitingJobsModel> waitingJobsModelArrayList;
    TextView txt_c_title;

    @Nullable
    @Override

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getActivity().getWindow();
            Drawable background = getActivity().getResources().getDrawable(R.color.view_border_color);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        return inflater.inflate(R.layout.trip_receipt_driver, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        ((MainActivity) getActivity()).setInitailSideBar(MainActivity.PANEL_SCHEDULED);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).setInitailSideBar(MainActivity.PANEL_SCHEDULED);
    }

    private void initView(View view) {
        view.findViewById(R.id.img_c_menu).setOnClickListener(this);
        txt_c_title = (TextView) view.findViewById(R.id.txt_c_title);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_menu:
                ((MainActivity) getActivity()).toggleSlider();
                break;
        }
    }
}
