package com.limocar.driver.firebase;


import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.limocar.driver.AppDelegate;
import com.limocar.driver.R;
import com.limocar.driver.activities.ForgotPasswordActivity;
import com.limocar.driver.constants.Tags;
import com.limocar.driver.net.Callback;
import com.limocar.driver.net.RestError;
import com.limocar.driver.net.SingletonRestClient;
import com.limocar.driver.utils.Prefs;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Belal on 5/27/2016.
 */
//Class extending FirebaseInstanceIdService
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    @Override
    public void onTokenRefresh() {
        //Getting registration token
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        //Displaying token on logcat
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        new Prefs(this).setGCMtoken(refreshedToken);
        new Prefs(this).setGCMtokeninTemp(refreshedToken);
        executeRegisterFCM(refreshedToken);
    }

    private void executeRegisterFCM(String refreshedToken) {
        if (AppDelegate.haveNetworkConnection(this)) {
            SingletonRestClient.get().registerDevice(refreshedToken, AppDelegate.getUUID(this), "Samsung", "Android", "6.0.0", new Callback<Response>() {
                @Override
                public void failure(RestError restError) {
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
                }

                @Override
                public void success(Response response, Response response2) {
                    JSONObject obj_json = null;
                    try {
                        obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                        if (obj_json.getString(Tags.s).contains("0")) {
                            new Prefs(MyFirebaseInstanceIDService.this).setDeviceRegistertoken("");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    private void sendRegistrationToServer(String token) {

        //You can implement this method to store the token on your server
        //Not required for current project
    }
}