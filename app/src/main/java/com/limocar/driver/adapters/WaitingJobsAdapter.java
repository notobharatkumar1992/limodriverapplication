package com.limocar.driver.adapters;


import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.limocar.driver.AppDelegate;
import com.limocar.driver.R;
import com.limocar.driver.fragments.HomeFragment;
import com.limocar.driver.fragments.TripReceiptFragment;
import com.limocar.driver.fragments.WaitingJobsFragment;
import com.limocar.driver.models.WaitingJobsModel;

import java.util.ArrayList;

import carbon.widget.ImageView;
import carbon.widget.TextView;


public class WaitingJobsAdapter extends RecyclerView.Adapter<WaitingJobsAdapter.ViewHolder> {
    public int position;
    private final ArrayList<WaitingJobsModel> waitingJobsModelArrayList;
    View v;
    FragmentActivity context;
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.waiting_jobs_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.txt_c_reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        holder.txt_c_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppDelegate.showFragmentAnimation(context.getSupportFragmentManager(), new HomeFragment(), R.id.main_containt);
            }
        });
    }

    public WaitingJobsAdapter(FragmentActivity context, ArrayList<WaitingJobsModel> newsModelArrayList) {
        this.context = context;
        this.waitingJobsModelArrayList = newsModelArrayList;
    }

    @Override
    public int getItemCount() {
        return 15;
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_c_reject, txt_c_accept, txt_c_pick_up, txt_c_drop_off;

        public ViewHolder(View itemView) {
            super(itemView);
            txt_c_reject = (TextView) itemView.findViewById(R.id.txt_c_reject);
            txt_c_accept = (TextView) itemView.findViewById(R.id.txt_c_accept);
            txt_c_pick_up = (TextView) itemView.findViewById(R.id.txt_c_pick_up);
            txt_c_drop_off = (TextView) itemView.findViewById(R.id.txt_c_drop_off);
        }
    }
}
