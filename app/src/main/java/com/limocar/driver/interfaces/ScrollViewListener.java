package com.limocar.driver.interfaces;

import com.limocar.driver.utils.ScrollViewExt;

/**
 * Created by Bharat on 07/01/2016.
 */
public interface ScrollViewListener {
    void onScrollChanged(ScrollViewExt scrollView, int x, int y, int oldx, int oldy);
}
