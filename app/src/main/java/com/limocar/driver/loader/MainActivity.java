package com.limocar.driver.loader;

/**
 * Created by Bharat on 17-Jan-17.
 */

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

import com.limocar.driver.R;

public class MainActivity extends Activity {

    ListView list;
    LazyAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        list = (ListView) findViewById(R.id.listView1);
        adapter = new LazyAdapter(this, imageUrls);
        list.setAdapter(adapter);
    }

    @Override
    public void onDestroy() {
        list.setAdapter(null);
        super.onDestroy();
    }

    private String imageUrls[] = {
            "http://sciencecouncil.org/web/wp-content/uploads/2015/10/scientists-landing-page-img.jpg",
            "http://previews.123rf.com/images/alexraths/alexraths1201/alexraths120100004/12027071-scientists-working-at-the-laboratory-Stock-Photo-experiment-science-chemical.jpg",
            "http://i.huffpost.com/gen/1868911/images/o-SCIENTIST-MAN-SMILE-facebook.jpg",
            "http://comps.canstockphoto.com/can-stock-photo_csp4567954.jpg",
            "http://st.depositphotos.com/1056393/1741/i/950/depositphotos_17416965-stock-photo-attractive-young-scientist-pipetting.jpg",
            "http://cliparts.co/cliparts/6ip/5q5/6ip5q5jBT.jpg",
            "http://obattradisionalkankerprostat.com/wp-content/uploads/2015/09/penelitian-1.jpg",
            "http://static01.global-free-classified-ads.com/user_images/8345680.jpg",
            "http://previews.123rf.com/images/alexraths/alexraths1503/alexraths150300030/37536863-scientist-working-at-the-laboratory-Stock-Photo-lab.jpg",
            "https://www.omicsonline.org/open-access/young-scientist-award/images/contentimage01.jpg",
            "http://f.tqn.com/y/chemistry/1/W/1/d/2/GettyImages-540813593.jpg",
            "http://www.laserhair-comb.com/images/medical_04.jpg",
            "https://31.media.tumblr.com/48d448da777b57038035399827001204/tumblr_inline_ndcv98Uu1R1sib46k.jpg",
            "http://www.evolutionnews.org/Lawyer,%20Scientist.jpg",
            "http://previews.123rf.com/images/alexraths/alexraths1009/alexraths100900003/7908037-group-of-scientists-working-at-the-laboratory-Stock-Photo-medical-scientist-computer.jpg",
            "https://si0.twimg.com/profile_images/60788468/androffice_bigger.png",
            "https://si0.twimg.com/profile_images/262620111/logodroid_bigger.png",
            "https://si0.twimg.com/profile_images/1024243227/Android-Apps_bigger.jpg",
            "https://si0.twimg.com/profile_images/2172264088/logo-testa-quad_bigger.png",
            "https://si0.twimg.com/profile_images/1186449790/mestre-android-twitter_bigger.jpg",
            "https://si0.twimg.com/profile_images/1785885571/androidvenezuela_bigger.png"
    };
}