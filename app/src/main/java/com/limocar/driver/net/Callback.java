package com.limocar.driver.net;

import retrofit.RetrofitError;

public abstract class Callback<T> implements retrofit.Callback<T> {

    public abstract void failure(RestError restError);

    @Override
    public void failure(RetrofitError error) {
        try {
            RestError restError = (RestError) error.getBodyAs(RestError.class);
            if (restError != null)
                failure(restError);
            else {
                failure(new RestError(error.getMessage()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}