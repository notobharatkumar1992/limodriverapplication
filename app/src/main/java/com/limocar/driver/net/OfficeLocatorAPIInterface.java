package com.limocar.driver.net;

import retrofit.client.Response;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Query;
import retrofit.mime.TypedFile;

public interface OfficeLocatorAPIInterface {


    @FormUrlEncoded
    @POST("/driver/login")
    void userLogin(@Field("u") String username,
                   @Field("p") String password,
                   @Field("uuid") String device_uuid,
                   @Field("device_type") String device_type,
                   @Field("notify_token") String notify_token,
                   Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/driver/register")
    void userRegistration(@Field("u") String username,
                          @Field("p") String password,
                          @Field("uuid") String device_uuid,
                          @Field("notify_token") String notify_token,
                          @Field("first_name") String first_name,
                          @Field("last_name") String last_name,
                          @Field("phone") String phone,
                          Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/driver/reset_password")
    void userForgetPassword(@Field("u") String username,
                            Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/driver/resetpassword")
    void userChangePassword(@Field("user_id") String user_id,
                            @Field("password") String password,
                            Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/driver/profile/update")
    void updateProfile(@Field("u") String username,
                       @Field("first_name") String first_name,
                       @Field("last_name") String last_name,
                       @Field("phone") String phone,
                       @Field("pic") String pic,
                       Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/driver/logout")
    void logout(@Field("session") String session, Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/device/register")
    void registerDevice(@Field("reg_id") String reg_id,
                        @Field("uuid") String uuid,
                        @Field("name") String name,
                        @Field("platform") String platform,
                        @Field("version") String version,
                        Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/driver/startTrip")
    void startTrip(@Field("session") String session, Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/driver/stopTrip")
    void stopTrip(@Field("session") String session, Callback<Response> responseCallback);


}

